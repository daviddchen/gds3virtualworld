﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockOnDoor : MonoBehaviour {
	private void Update() {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit, 1.0f)) {
				var doorknock = GameObject.Find ("doorknock");
				var frogonlogcroaker = GameObject.Find ("frogonlogcroaker");
				if (hit.transform == (doorknock.transform)) {
					FindObjectOfType<AudioManager>().Play ("KnockOnDoor");
				}
				if (hit.transform == (frogonlogcroaker.transform)) {
					FindObjectOfType<AudioManager>().Play ("Croak");
				}
			}
		}
	}
	private void PrintName(GameObject go) {
		print (go.name);
	}
}
