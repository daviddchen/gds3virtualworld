=========================
===== VIRTUAL WORLD =====
=========================
(because why live in the real world when you can hang out with low poly frogs???)

Hi,

I'm still not good at Maya or Unity. I know it's kinda barren but it's calming at least... I think...

Oh, I made the music! It doesn't really fit with the vibe I was going for but just imagine 
the atmosphere's a bit more magical and spirit world-y. 

SOURCE CONTROL:
https://bitbucket.org/daviddchen/gds3virtualworld/

External resources:
Asset store:
- BOXOPHOBIA (Skybox)
SFX:
- bullfrog_croak.wav (http://www.freesfx.co.uk/sfx/frog, edited in Audacity)
- knockondoor.wav (https://freesound.org/people/HunteR4708/sounds/256513/, edited in Audacity)
Code:
- Omnirift's Raycast Clicking tutorial (https://www.youtube.com/watch?v=EANtTI6BCxk)
- Brackey's AudioManager Tutorial (https://www.youtube.com/watch?v=6OT43pvUyfY)